import studio.athena.objectpool.pattern.client.grabBooker;
import studio.athena.objectpool.pattern.objectpool.GrabPool;

public class GrabApp {
	public static final int NUM_OF_CLIENT = 10;
	 
    public static void main(String[] args) {
        GrabPool grabPool = new GrabPool();
        grabPool.createGrabCar();
        for (int i = 1; i <= NUM_OF_CLIENT; i++) {
            Runnable booker = new grabBooker(grabPool);
            Thread thread = new Thread(booker);
            thread.start();
        }
    }
}
