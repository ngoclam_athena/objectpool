package studio.athena.objectpool.pattern.objectpool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import studio.athena.objectpool.pattern.exception.TaxiNotFoundException;
import studio.athena.objectpool.pattern.object.Grabcar;

public class GrabPool {
	private static final long EXPIRED_TIME_IN_MILISECOND = 2000;
	private static final int NUMBER_OF_GRABCAR = 4;
	
	private final List<Grabcar> availableCars = Collections.synchronizedList(new ArrayList<Grabcar>());
	private final List<Grabcar> inUseCars = Collections.synchronizedList(new ArrayList<Grabcar>());
	private final AtomicInteger count = new AtomicInteger(0);
    private final HashMap<String, Boolean> waiting = new HashMap<String, Boolean>();
    
    public Grabcar getGrabcar(String guess_name) {
    	if (!availableCars.isEmpty()) {
    		Grabcar grabcar = availableCars.remove(0);
    		addGuessToGrabCar(guess_name, grabcar);
    		return grabcar;
    	}
    	if (count.get() == NUMBER_OF_GRABCAR) {
    		waitingUntilTaxiAvailable(guess_name);
    		return this.getGrabcar(guess_name);
    	}
    	this.createGrabCar();
    	return this.getGrabcar(guess_name);
    }
    
    private void addGuessToGrabCar(String guess_name, Grabcar grabcar) {
    	grabcar.serveCurrentGuess(guess_name);
    	System.out.println(grabcar.getStatus());
    	inUseCars.add(grabcar);
    }
    
    public void createGrabCar() {
    	for (int i = 0 ; i < NUMBER_OF_GRABCAR ; i++) {    		
    		Grabcar grabcar = new Grabcar("Grabcar " + count.incrementAndGet());
    		availableCars.add(grabcar);
    		System.out.println(grabcar.getName() + " is created");
    	}
    } 

    public void release(Grabcar grabcar) {
    	inUseCars.remove(grabcar);
    	grabcar.finishCurrentGuess();
    	System.out.println(grabcar.getStatus());
    	availableCars.add(grabcar);
    }
    
    private void waitingUntilTaxiAvailable(String guess_name) {
    	if (waiting.containsKey(guess_name)) {
	        if (waiting.get(guess_name)) {
	            waiting.put(guess_name, false);
	            waiting.remove(guess_name);
	            throw new TaxiNotFoundException("No taxi available");
	        }
    	}
        waiting.put(guess_name, true);
        waitingForProcessing(EXPIRED_TIME_IN_MILISECOND);
    }
    
    private void waitingForProcessing(long numberOfSecond) {
        try {
            TimeUnit.MILLISECONDS.sleep(numberOfSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
    
    
}
