package studio.athena.objectpool.pattern.object;

public class Grabcar {
	private String name;
	private String current_guess;
	public Grabcar(String name) {
		this.name = name;
		this.current_guess = "available";
	}
	
	public String getName() {
		return this.name;
	}
	
	public void serveCurrentGuess(String guess_name) {
		this.current_guess = guess_name;
	}
	
	public void finishCurrentGuess() {
		this.current_guess = "available";
	}
	
	public String getStatus() {
		String status = this.current_guess == "available" ? "available" : ("serving "+this.current_guess);
		return ( "Grabcar [name=" + this.name + ("] is " + status));  
	}
}
