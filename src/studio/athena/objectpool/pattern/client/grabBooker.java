package studio.athena.objectpool.pattern.client;

import java.util.concurrent.TimeUnit;
import java.util.Random;

import studio.athena.objectpool.pattern.exception.TaxiNotFoundException;
import studio.athena.objectpool.pattern.object.Grabcar;
import studio.athena.objectpool.pattern.objectpool.GrabPool;

public class grabBooker implements Runnable {
	private GrabPool grabPool;
	
	public grabBooker(GrabPool grabPool) {
		this.grabPool = grabPool;
	}
	
	@Override
	public void run() {
		takeGrabCar();
	}
	
	private void takeGrabCar() {
		try {
			String client_name = Thread.currentThread().getName();
			System.out.println("New client: " + client_name);
			Grabcar grabcar = this.grabPool.getGrabcar(client_name);
			int time = randInt(1500, 2500);
			TimeUnit.MILLISECONDS.sleep(time);
			grabPool.release(grabcar);
		} catch (InterruptedException | TaxiNotFoundException e) {
        System.out.println(">>>Rejected the client: " + Thread.currentThread().getName());
		}
	}
	
	public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
